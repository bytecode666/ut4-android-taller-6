package dam.android.silvia.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    // TODO ejercicio 9.1 Pongo un arraylist de objetos ContactItem
    private ArrayList<ContactItem> myContacts;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        // TODO ejercicio 9.1 pongo cada elemento presente en el layout de la vista de contactos.
        TextView tvID;
        TextView tvNombreContacto;
        TextView tvNumeroTel;
        ImageView imagenContacto;
        View view;

        // TODO ejercicio 9.1 le paso un view en vez de un textview al contructor. Busco cada elemento por su id
        public MyViewHolder(View view) {

            super(view);
            this.view = view;
            this.tvID = view.findViewById(R.id.tvID);
            this.tvNombreContacto = view.findViewById(R.id.tvNombreContacto);
            this.tvNumeroTel = view.findViewById(R.id.tvNumeroTel);
            this.imagenContacto = view.findViewById(R.id.ivContacto);

        }

        /* TODO ejercicio 9.1 Asigno a cada textview los datos correspondientes del contacto,
            y su imagen si esta presente, y si no, una por defecto*/
        public void bind(ContactItem contactData) {

            this.tvID.setText(contactData.getIdContact());
            this.tvNombreContacto.setText(contactData.getContactName());
            this.tvNumeroTel.setText(contactData.getContactNumber());

            if (contactData.getContactThumbnailPhoto() != null) {

                this.imagenContacto.setImageURI(Uri.parse(contactData.getContactThumbnailPhoto()));

            } else {

                this.imagenContacto.setImageResource(R.mipmap.ic_launcher);
            }
        }
    }

    // TODO ejercicio 9.1 paso al constructor un arraylist de ContactItem y lo guardo en el arraylist de esta clase.
    MyAdapter(ArrayList<ContactItem> myContacts) {

        this.myContacts = myContacts;
    }

    /* TODO ejercicio 9.1 guardo en un view cada elemento de la vista a asignar al recycler view,
        y posteriormente, devuelvo el viewholder con la vista*/
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_view, parent, false);
        return new MyViewHolder(view);
    }

    // TODO ejercicio 9.1 obtengo la posicion del item del arraylist pasandole al metodo get la posicion.
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(myContacts.get(position));
    }


    // TODO ejercicio 9.1 devuelvo el tamaño del arraylist.
    @Override
    public int getItemCount() {
        return myContacts.size();
    }
}