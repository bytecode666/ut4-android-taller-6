package dam.android.silvia.u4t6contacts;

// TODO ejercicio 9.1 Clase donde guardamos todos los datos de cada contacto.
public class ContactItem {

    private String contactName;
    private String contactNumber;
    private String idData;
    private String idContact;
    private String contactLookupKey;
    private String contactPhoneType;
    private String contactThumbnailPhoto;
    private String rawContactId;


    public ContactItem(String contactName, String contactNumber, String idData, String idContact, String contactLookupKey,
                       String contactPhoneType, String contactThumbnailPhoto, String rawContactId) {

        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.idData = idData;
        this.idContact = idContact;
        this.contactLookupKey = contactLookupKey;
        this.contactPhoneType = contactPhoneType;
        this.contactThumbnailPhoto = contactThumbnailPhoto;
        this.rawContactId = rawContactId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getIdData() {
        return idData;
    }

    public String getIdContact() {
        return idContact;
    }

    public String getContactLookupKey() {
        return contactLookupKey;
    }

    public String getContactPhoneType() {
        return contactPhoneType;
    }

    public String getContactThumbnailPhoto() {
        return contactThumbnailPhoto;
    }

    public String getRawContactId() {
        return rawContactId;
    }


}