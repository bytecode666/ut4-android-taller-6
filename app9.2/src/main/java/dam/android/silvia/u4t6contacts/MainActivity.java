package dam.android.silvia.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener, MyAdapter.OnItemLongClickListener {

    MyContacts myContacts;
    RecyclerView recyclerView;
    TextView tvInformationContact;


    // Permisos requeridos para el contact provider. Solo se otorga el permiso de lectura
    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};

    // Id para identificar los permisos de lectura de contactos
    private static final int REQUEST_CONTACTS = 1;

    // TODO ejercicio 9.2 constante donde guardo el codigo de request del contacto seleccionado.
    private static final int REQUEST_SELECTED_CONTACT = 1;

    private MyAdapter myAdapter;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if (checkPermissions()) {
            setListAdapter();
        }
    }

    private void setUI() {

        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,
                false));

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        tvInformationContact = findViewById(R.id.tvInfoContact);
        ocultarTextView();
    }

    private void setListAdapter() {

        myContacts = new MyContacts(this);
        myAdapter = new MyAdapter(myContacts.getContacts(), this, this);
        recyclerView.setAdapter(myAdapter);

        // Ocultar la lista de contactos cuando esta vacia.
        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);

    }

    // TODO ejercicio 9.2 ocultamos el textview cada vez que se hace scroll en el recycler view
    public void ocultarTextView() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                tvInformationContact.setVisibility(View.INVISIBLE);
            }
        });
    }

    private boolean checkPermissions() {

        // Se comprueba los permisos concedidos. Si se han aprobado, devuelve true y si han sido revocados, devuelve false.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;

        } else {

            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        // Si el permiso ha sido concedido, se muestra la lista de contactos, si se ha rechazado, mostramos el toast.
        if (requestCode == REQUEST_CONTACTS) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setListAdapter();

            } else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // TODO ejercicio 9.2 listener de pulsacion corta donde mostramos el textview con los datos del contacto
    @Override
    public void onContactClick(ContactItem contactItem) {

        tvInformationContact.setText(contactItem.toString());
        tvInformationContact.setVisibility(View.VISIBLE);
    }

    // TODO ejercicio 9.2 listener de pulsacion larga donde mostramos el contacto desde la aplicacion de contactos mediante un intent.
    @Override
    public boolean onContactLongClick(ContactItem contactItem) {

        Uri selectedContactUri = ContactsContract.Contacts.getLookupUri(Long.parseLong(contactItem.getIdData()), contactItem.getContactLookupKey());
        intent = new Intent(Intent.ACTION_VIEW, selectedContactUri);
        startActivityForResult(intent, REQUEST_SELECTED_CONTACT);

        return true;
    }

    // TODO ejercicio 9.2 metodo donde actualizamos todos los contactos si el requestcode del intent es igual a 1.
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == REQUEST_SELECTED_CONTACT) {
            myAdapter.actualizarContactos(myContacts.getContacts());
        }
    }
}