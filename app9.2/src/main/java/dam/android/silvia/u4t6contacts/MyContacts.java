package dam.android.silvia.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();
    }

    public ArrayList<ContactItem> getContacts() {
        ArrayList<ContactItem> contactsList = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Contacts._ID, ContactsContract.Contacts.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='"
                + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND "
                + ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projection,
                selectionFilter, null, ContactsContract.Data.DISPLAY_NAME);

        if (contactsCursor != null) {

            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);

            int contactDataIDIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int contactIDIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID);
            int lookupKeyIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.LOOKUP_KEY);
            int rawContactIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int phoneTypeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int photoThumbIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);


            while (contactsCursor.moveToNext()) {


                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);

                String dataID = contactsCursor.getString(contactDataIDIndex);
                String contactID = contactsCursor.getString(contactIDIndex);
                String rawContactId = contactsCursor.getString(rawContactIdIndex);
                String phoneType = contactsCursor.getString(phoneTypeIndex);
                String photoThumb = contactsCursor.getString(photoThumbIndex);
                String lookupKey = contactsCursor.getString(lookupKeyIndex);

                ContactItem contactItem = new ContactItem(name, number, dataID, contactID, lookupKey,
                        phoneType, photoThumb, rawContactId);

                contactsList.add(contactItem);
            }

            contactsCursor.close();
        }

        return contactsList;
    }

    public ContactItem getContactData(int position) {
        return myDataSet.get(position);
    }

    public int getCount() {
        return myDataSet.size();
    }
}