package dam.android.silvia.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    // TODO ejercicio 9.2 interface donde definimos el metodo del listener OnItemClickListener.
    public interface OnItemClickListener {
        void onContactClick(ContactItem contactItem);
    }

    // TODO ejercicio 9.2 interface donde definimos el metodo del listener OnItemLongClickListener.
    public interface OnItemLongClickListener {
        boolean onContactLongClick(ContactItem contactItem);
    }

    private ArrayList<ContactItem> myContacts;
    private OnItemClickListener listenerClick;
    private OnItemLongClickListener listenerLongClick;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvID;
        TextView tvNombreContacto;
        TextView tvNumeroTel;
        ImageView imagenContacto;
        View view;

        public MyViewHolder(View view) {

            super(view);
            this.view = view;
            this.tvID = view.findViewById(R.id.tvID);
            this.tvNombreContacto = view.findViewById(R.id.tvNombreContacto);
            this.tvNumeroTel = view.findViewById(R.id.tvNumeroTel);
            this.imagenContacto = view.findViewById(R.id.ivContacto);

        }

        public void bind(final ContactItem contactData, final OnItemClickListener listenerClick, OnItemLongClickListener listenerLongClick) {

            this.tvID.setText(contactData.getIdContact());
            this.tvNombreContacto.setText(contactData.getContactName());
            this.tvNumeroTel.setText(contactData.getContactNumber());

            if (contactData.getContactThumbnailPhoto() != null) {

                this.imagenContacto.setImageURI(Uri.parse(contactData.getContactThumbnailPhoto()));

            } else {

                this.imagenContacto.setImageResource(R.mipmap.ic_launcher);
            }

            view.setOnClickListener(v -> listenerClick.onContactClick(contactData));
            view.setOnLongClickListener(v -> listenerLongClick.onContactLongClick(contactData));
        }
    }

    MyAdapter(ArrayList<ContactItem> myContacts, OnItemClickListener listenerClick, OnItemLongClickListener onItemLongClickListener) {

        this.myContacts = myContacts;
        this.listenerClick = listenerClick;
        this.listenerLongClick = onItemLongClickListener;
    }

    /* TODO ejercicio 9.2 metodo donde vacio la lista y la vuelvo a llenar con la lista pasada por parametro.
        Se notifica al recycler view de los cambios*/
    public void actualizarContactos(ArrayList<ContactItem> contactItem) {

        myContacts.clear();
        myContacts.addAll(contactItem);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(myContacts.get(position), listenerClick, listenerLongClick);
    }


    @Override
    public int getItemCount() {
        return myContacts.size();
    }
}