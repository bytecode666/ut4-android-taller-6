package dam.android.silvia.u4t6contacts;

// TODO ejercicio 9.1 Clase donde guardamos todos los datos de cada contacto.
public class ContactItem {

    private String contactName;
    private String contactNumber;
    private String idData;
    private String idContact;
    private String contactLookupKey;
    private String contactPhoneType;
    private String contactThumbnailPhoto;
    private String rawContactId;

    private static final String TYPE_HOME = "1";
    private static final String TYPE_WORK = "3";
    private static final String TYPE_MOBILE = "2";

    public ContactItem(String contactName, String contactNumber, String idData, String idContact, String contactLookupKey,
                       String contactPhoneType, String contactThumbnailPhoto, String rawContactId) {

        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.idData = idData;
        this.idContact = idContact;
        this.contactLookupKey = contactLookupKey;
        this.contactPhoneType = contactPhoneType;
        this.contactThumbnailPhoto = contactThumbnailPhoto;
        this.rawContactId = rawContactId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getIdData() {
        return idData;
    }

    public String getIdContact() {
        return idContact;
    }

    public String getContactLookupKey() {
        return contactLookupKey;
    }

    public String getContactPhoneType() {
        return contactPhoneType;
    }

    public String getContactThumbnailPhoto() {
        return contactThumbnailPhoto;
    }

    public String getRawContactId() {
        return rawContactId;
    }

    // TODO EJERCICIO 9.2 devolvemos los datos del contacto mediante una string.
    public String toString() {

        // TODO ejercicio 9.2 llamo al metodo que me devuelve la string del tipo del telefono aqui
        return this.contactName + "  " + this.contactNumber + "  " + phoneType() + "  _ID: " + this.idData
                + System.lineSeparator() + " CONTACT_ID: " + this.idContact + " RAW_CONTACT_ID: " + this.rawContactId
                + System.lineSeparator() + " LOOKUP_KEY: " + this.contactLookupKey;
    }

    // TODO ejercicio 9.2 segun el tipo que sea (1,2,3, o 7 devuelvo home o mobile o other)
    public String phoneType() {

        String contactType = null;

        switch (this.contactPhoneType) {

            case TYPE_HOME:
                contactType = "(HOME)";
                break;

            case TYPE_MOBILE:
                contactType = "(MOBILE)";
                break;

            case TYPE_WORK:
                contactType = "(WORK)";
                break;

            default:
                contactType = "(OTHER)";
        }

        return contactType;
    }
}